# Hackdays September 2022 Splunk Soar Batched Queries

## To Do

* Enrich action needs to be updated to support multiple domain inputs. I hacked this into the app during hackdays
* Might want to include locking during queue operations (wich SOAR has an api for). If we're worried about multiple runs in parallel.
* Need to add queue clean after enrich operation
* Probably want to output enrich information so it can be linked to other playbooks
* No error handling
* Probably don't want to lookup duplicate domains if they exist in different artifacts
* Probably want an additional playbook to manually process queue