"""

"""


import phantom.rules as phantom
import json
from datetime import datetime, timedelta


def on_start(container):
    phantom.debug('on_start() called')

    # call 'add_artifact_to_queue' block
    add_artifact_to_queue(container=container)

    return

def add_artifact_to_queue(action=None, success=None, container=None, results=None, handle=None, filtered_artifacts=None, filtered_results=None, custom_function=None, **kwargs):
    phantom.debug("add_artifact_to_queue() called")

    container_artifact_data = phantom.collect2(container=container, datapath=["artifact:*.id","artifact:*.cef.destinationDnsDomain"])

    container_artifact_header_item_0 = [item[0] for item in container_artifact_data]
    container_artifact_cef_item_1 = [item[1] for item in container_artifact_data]

    add_artifact_to_queue__queue_length = None

    ################################################################################
    ## Custom Code Start
    ################################################################################

    # Write your custom code here...
    # @todo: don't insert duplicates
    # @todo: check different domain fields
    queue = phantom.get_list(list_name="Iris Investigate Queue", column_index=1)
    queued_artifacts = []
    for item in queue[2]:
        queued_artifacts.append(item[0])
    queued_artifacts = set(queued_artifacts)
    phantom.debug("dt debug - artifacts in queue: {0}".format(queued_artifacts))
    phantom.debug("dt debug - artifacts in event: {0}".format(container_artifact_header_item_0))
    
    for i in range(len(container_artifact_header_item_0)):
        artifact_id = container_artifact_header_item_0[i]
        domain = container_artifact_cef_item_1[i]
        if str(artifact_id) in queued_artifacts:
            phantom.debug("dt debug - not adding artifact: {0} - {1} to queue".format(artifact_id, domain))
        else:
            phantom.debug("dt debug - adding artifact: {0} - {1} to queue".format(artifact_id, domain))
            phantom.add_list(list_name="Iris Investigate Queue", values=[artifact_id, domain])
            
    # @todo add new artifacts to queue length
    add_artifact_to_queue__queue_length = len(queue)
    

    ################################################################################
    ## Custom Code End
    ################################################################################

    phantom.save_run_data(key="add_artifact_to_queue:queue_length", value=json.dumps(add_artifact_to_queue__queue_length))

    decision_1(container=container)

    return


def decision_1(action=None, success=None, container=None, results=None, handle=None, filtered_artifacts=None, filtered_results=None, custom_function=None, **kwargs):
    phantom.debug("decision_1() called")

    # check for 'if' condition 1
    found_match_1 = phantom.decision(
        container=container,
        conditions=[
            ["add_artifact_to_queue:custom_function:queue_length", ">=", 3]
        ])

    # call connected blocks if condition 1 matched
    if found_match_1:
        build_domain_batch(action=action, success=success, container=container, results=results, handle=handle)
        return

    return


def build_domain_batch(action=None, success=None, container=None, results=None, handle=None, filtered_artifacts=None, filtered_results=None, custom_function=None, **kwargs):
    phantom.debug("build_domain_batch() called")

    build_domain_batch__domains_to_enrich = None

    ################################################################################
    ## Custom Code Start
    ################################################################################

    build_domain_batch__domains_to_enrich = []
    queue = phantom.get_list(list_name="Iris Investigate Queue")
    for artifact in queue[2]:
        phantom.debug("dt debug - artifact to enrich: {0}".format(artifact))
        build_domain_batch__domains_to_enrich.append(artifact[1])
    phantom.debug("dt debug - domain batch: {0}".format(build_domain_batch__domains_to_enrich))
    
    ################################################################################
    ## Custom Code End
    ################################################################################

    phantom.save_run_data(key="build_domain_batch:domains_to_enrich", value=json.dumps(build_domain_batch__domains_to_enrich))

    enrich_domain_1(container=container)

    return


def update_artifacts(action=None, success=None, container=None, results=None, handle=None, filtered_artifacts=None, filtered_results=None, custom_function=None, **kwargs):
    phantom.debug("update_artifacts() called")

    passthrough_1_data = phantom.collect2(container=container, datapath=["passthrough_1:custom_function_result.data.*.item"])

    passthrough_1_data___item = [item[0] for item in passthrough_1_data]

    ################################################################################
    ## Custom Code Start
    ################################################################################

    # Write your custom code here...
    enrich_data = passthrough_1_data[0][0]
    queue = phantom.get_list(list_name="Iris Investigate Queue", column_index=1)
    queued_artifacts = []
    for item in queue[2]:
        artifact_id = item[0]
        domain = item[1]
        domain_data = next((item for item in enrich_data if item['domain'] == domain), {})
        phantom.debug("dt debug - add note for {0}".format(domain))
        phantom.add_note(note_type='general', 
                         title='Iris Enrich {0}'.format(domain), 
                         content="risk for: {0} is : {1}".format(domain, domain_data.get('domain_risk', {}).get('risk_score')),
                         artifact_id=artifact_id)
    ################################################################################
    ## Custom Code End
    ################################################################################

    return


def enrich_domain_1(action=None, success=None, container=None, results=None, handle=None, filtered_artifacts=None, filtered_results=None, custom_function=None, **kwargs):
    phantom.debug("enrich_domain_1() called")

    # phantom.debug('Action: {0} {1}'.format(action['name'], ('SUCCEEDED' if success else 'FAILED')))

    domain_formatted_string = phantom.format(
        container=container,
        template="""{0}\n""",
        parameters=[
            ""
        ])

    parameters = []

    if domain_formatted_string is not None:
        parameters.append({
            "domain": domain_formatted_string,
        })

    ################################################################################
    ## Custom Code Start
    ################################################################################

    # Write your custom code here...
    # @todo figure out how to pass list of domains from previous actions. possibly create temp artifact?
    domains_to_enrich = phantom.get_run_data(key='build_domain_batch:domains_to_enrich')
    domain_string = ','.join(json.loads(domains_to_enrich))

    parameters = []
    parameters.append({'domain': domain_string})

    ################################################################################
    ## Custom Code End
    ################################################################################

    phantom.act("enrich domain", parameters=parameters, name="enrich_domain_1", assets=["domaintoolsiriscreds"], callback=passthrough_1)

    return


def passthrough_1(action=None, success=None, container=None, results=None, handle=None, filtered_artifacts=None, filtered_results=None, custom_function=None, **kwargs):
    phantom.debug("passthrough_1() called")

    enrich_domain_1_result_data = phantom.collect2(container=container, datapath=["enrich_domain_1:action_result.data","enrich_domain_1:action_result.parameter.context.artifact_id"], action_results=results)

    enrich_domain_1_result_item_0 = [item[0] for item in enrich_domain_1_result_data]

    parameters = []

    parameters.append({
        "input_1": enrich_domain_1_result_item_0,
        "input_2": None,
        "input_3": None,
        "input_4": None,
        "input_5": None,
        "input_6": None,
        "input_7": None,
        "input_8": None,
        "input_9": None,
        "input_10": None,
    })

    ################################################################################
    ## Custom Code Start
    ################################################################################

    # Write your custom code here...

    ################################################################################
    ## Custom Code End
    ################################################################################

    phantom.custom_function(custom_function="community/passthrough", parameters=parameters, name="passthrough_1", callback=update_artifacts)

    return


def on_finish(container, summary):
    phantom.debug("on_finish() called")

    ################################################################################
    ## Custom Code Start
    ################################################################################

    # This function is called after all actions are completed.
    # summary of all the action and/or all details of actions
    # can be collected here.

    # summary_json = phantom.get_summary()
    # if 'result' in summary_json:
        # for action_result in summary_json['result']:
            # if 'action_run_id' in action_result:
                # action_results = phantom.get_action_results(action_run_id=action_result['action_run_id'], result_data=False, flatten=False)
                # phantom.debug(action_results)

    ################################################################################
    ## Custom Code End
    ################################################################################

    return